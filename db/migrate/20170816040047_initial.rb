class Initial < ActiveRecord::Migration[5.1]
  def change
    create_table :tags do |t|
      t.string :name, null: false
      t.timestamps
    end

    create_table :entities do |t|
      t.string :entity_type, null: false
      t.string :public_id, null: false
      t.timestamps
    end

    create_table :entities_tags do |t|
      t.integer :tag_id
      t.integer :entity_id
      t.timestamps
    end

    add_index :tags, :name
    add_index :entities, [:public_id, :entity_type]
    add_index :entities_tags, [:tag_id, :entity_id]
  end
end
