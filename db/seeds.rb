Entity.create(
  entity_type: "Article",
  public_id: "abc123",
  tags: [
    Tag.find_or_create_by(name: "Pink"),
    Tag.find_or_create_by(name: "Bike"),
  ]
)
Entity.create(
  entity_type: "Article",
  public_id: "xyz987",
  tags: [
    Tag.find_or_create_by(name: "Green"),
    Tag.find_or_create_by(name: "Bike"),
  ]
)
Entity.create(
  entity_type: "Book",
  public_id: "abc123",
  tags: [
    Tag.find_or_create_by(name: "Bike"),
    Tag.find_or_create_by(name: "Green"),
  ]
)

