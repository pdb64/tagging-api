Rails.application.routes.draw do
  post "/tags", controller: :tags, action: :create

  get "/entities/:entity_type/:public_id", controller: :entities, action: :show
  delete "/entities/:entity_type/:public_id", controller: :entities, action: :destroy

  get "/stats/tags", controller: :stats, action: :tags
  get "/stats/entities", controller: :stats, action: :entities
end
