# README

Hey there! First of all, thank you for looking at my submission. I had fun
making this api so I hope you enjoy reading it as well.

## Getting Started

If you're using `rvm` just entering the project directory should create a
"tagging_api" gemset and switch to the correct ruby version (2.4.1). After that
you should be able to install `bundler` and then `bundle install` to get all of
the project's dependencies.

To run the spec suite, run `rake` or `rspec`.

To spin up the application, run `rails server`.

## Design Considerations

For technology I chose Ruby on Rails' stripped down `--api`. I also opted out
of many features (action mailer, action cable, coffeescript) because I knew
they would not be needed for this project.

The modeling I went with is very simple: A `entities` table, a `tags` table and
a join table between them. Appropriate indexes are added. Ultimately, it seems
like `entities` wants to be a polymorphic relationship (so hypothetical
`articles` and `books` tables would be `taggable`), but for this challenge it
was important to tag arbitrarily named entities so I decided against it.

You may notice that all of the logic exists in the controllers. Normally, I
would consider this a red flag, but the logic wasn't complicated enough to
justify breaking out service objects (at least in my opinion). The notable
exception to this is the TagsController, where I did feel like the complexity
justified a service object.

The testing strategy I took was to write controller-level integration tests.
I'm dislike model-level tests that just test that Rails works, and the models
and their relationships are tested implicitly via the controller tests. I could
of wrote unit tests for the TagCreationService, but it wouldn't add any more
coverage so I decided against it.

I took some artistic liberties with the challenge. Notably:

- The `/tags/:entity_type/:entity_id` endpoints were changed to
  `/entities/:entity_type/:public_id`. This was because I felt like we were
  interacting with the entities resource more than the tags resource.
- The `/stats` endpoint was changed to `/stats/tags`.
- The `/stats/:entity_type/:entity_id` endpoint was changed to
  `/stats/entities`. I felt that it was a little boring to only get the stats
  for a single entity (we already get most of that information from the GET
  endpoint) so instead I get stats for all entities. Much cooler!
