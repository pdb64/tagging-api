class StatsController < ApplicationController
  def tags
    stats = Tag.all.order(:name).map do |t|
      {tag: t.name, count: t.entities.count}
    end

    render json: stats
  end

  def entities
    memo = {}
    Entity.all.each do |e|
      memo[e.entity_type] ||= 0
      memo[e.entity_type] += 1
    end

    stats = memo.map do |k, v|
      {entity_type: k, count: v}
    end

    render json: stats
  end
end
