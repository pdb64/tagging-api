class EntitiesController < ApplicationController
  def show
    entity = Entity.find_by(
      public_id: params[:public_id],
      entity_type: params[:entity_type],
    )

    if entity
      render json: {
        public_id: entity.public_id,
        entity_type: entity.entity_type,
        tags: entity.tags.map(&:name),
      }
    else
      head :not_found
    end
  end

  def destroy
    entity = Entity.find_by(
      public_id: params[:public_id],
      entity_type: params[:entity_type],
    )

    if entity
      entity.tags.each { |t| t.destroy }
      entity.destroy!
      head :ok
    else
      head :not_found
    end
  end
end
